import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;


public class ChartStatistics extends Application {

    @Override public void start(Stage stage) {
        stage.setTitle("Граыикалык статистика");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Журнал");

        final LineChart<String,Number> lineChart =
                new LineChart<String,Number>(xAxis,yAxis);

        lineChart.setTitle("Графикалык статистика");

        XYChart.Series series = new XYChart.Series();
        series.setName("Corpus");

        series.getData().add(new XYChart.Data("Ала-Тоо 2009", 230));
        series.getData().add(new XYChart.Data("Ала-Тоо 2010", 1403));
        series.getData().add(new XYChart.Data("Ала-Тоо 2011", 152));
        series.getData().add(new XYChart.Data("Ала-Тоо 2012", 244));
        series.getData().add(new XYChart.Data("Ала-Тоо 2013", 345));
        series.getData().add(new XYChart.Data("Шоокум", 3643));
        series.getData().add(new XYChart.Data("Акбашат", 2244));


        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}