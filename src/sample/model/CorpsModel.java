package sample.model;

import javafx.beans.property.*;

/**
 * Created by Che on 4/14/14.
 */
public class CorpsModel {
	String word;
	int frequency;

	public CorpsModel(String word, int frequency) {
		this.word = word;
		this.frequency = frequency;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
}
