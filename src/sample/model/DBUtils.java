package sample.model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Che on 3/28/14.
 */
public class DBUtils {
    public static final String EMBEDDED_USER = "olyanren";
    public static final String EMBEDDED_USER_PASSWORD = "1987";
    public static final int PORT_NUMBER = Integer.parseInt(System.getProperty("c-mxj_test_port", "3306"));
    public static final String H_DRIVER = "org.hsqldb.jdbcDriver";
    public static final String M_DRIVER = "com.mysql.jdbc.Driver";
    private static final String USERNAME = "SA";
    private static final String PASSWORD = "1";
    private static final String DATABASE_NAME = "journals";
    private static final String M_CONN_STRING = "jdbc:mysql://localhost:3366/" + DATABASE_NAME + "?useUnicode=true&characterEncoding=UTF8";
    private static final String EMBEDDED_MYSQL_CONN = "jdbc:mysql://localhost:" + PORT_NUMBER + "/" + DATABASE_NAME + "?" + "createDatabaseIfNotExist=true&useUnicode=true&characterEncoding=UTF8";
    public static MYSQLServer mysqlServer = new MYSQLServer();
    /**
     * Для встороенной базы данных HSQLDB, необходимо указать <code>res</code> вместо <code>file</code>
     */
    private static String H_CONN_STRING = "jdbc:hsqldb:file:C:/DB/" + DATABASE_NAME;

    public static Connection getConnection(DBType dbType) throws SQLException, ClassNotFoundException, IOException {
        switch (dbType) {
            case MYSQL:
                Class.forName(M_DRIVER);
                return DriverManager.getConnection(M_CONN_STRING, USERNAME, PASSWORD);
            case EMBEDDED_MYSQL:
                mysqlServer.startServer();
                Class.forName(M_DRIVER);
                return DriverManager.getConnection(EMBEDDED_MYSQL_CONN, EMBEDDED_USER, EMBEDDED_USER_PASSWORD);
            case HSQLDB:
                Class.forName(H_DRIVER);
                return DriverManager.getConnection(H_CONN_STRING, USERNAME, PASSWORD);
            default:
                return null;
        }
    }
}
