package sample.model;

import com.mysql.management.MysqldResource;
import com.mysql.management.MysqldResourceI;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Marvell on 28.05.2015.
 */
public class MYSQLServer {
    final static String JAVA_IO_TMPDIR = "java.io.tmpdir";
    private static File ourAppDir = new File(System.getProperty(JAVA_IO_TMPDIR));
    private static File databaseDir = new File(ourAppDir, "mysql-server");
    private static File dataDir = new File(databaseDir, "data");
    private static File tablesDir = new File(dataDir, "test/sources.json");
    private static File databaseDumpFileDir = new File(dataDir, "test/journals.sql");
    MysqldResource mysqldResource;

    public static File getTablesDir() {
        return tablesDir;
    }

    public void startServer() {
        mysqldResource = launchDeamon(databaseDir, DBUtils.PORT_NUMBER, DBUtils.EMBEDDED_USER, DBUtils.EMBEDDED_USER_PASSWORD);
        String route = databaseDir.getPath() + "\\bin\\";
        try {
            System.out.println("LAUNCHING...");
            Process process = Runtime.getRuntime().exec(route + "mysql -u olyanren -p1987 -e \"use journals; source " + databaseDumpFileDir.getCanonicalFile().toString().replace("\\", "//") + ";\"");
            int status = process.waitFor();
            System.out.println("Congrats! Status is: " + status);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public MysqldResource launchDeamon(File databaseDir, int port, String userName, String password) {
        MysqldResource mysqldResource = new MysqldResource(databaseDir);
        Map database_options = new HashMap();
        database_options.put(MysqldResourceI.PORT, Integer.toString(port));
        database_options.put(MysqldResourceI.INITIALIZE_USER, "true");
        database_options.put(MysqldResourceI.INITIALIZE_USER_NAME, userName);
        database_options.put(MysqldResourceI.INITIALIZE_PASSWORD, password);

        mysqldResource.start("test-mysqld-thread", database_options);
        if (!mysqldResource.isRunning()) {
            throw new RuntimeException("MySQL did not start.");
        }
        System.out.println("MySQL is running.");
        return mysqldResource;
    }

    public void stopServer() {
        mysqldResource.shutdown();
    }
}
