package sample.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static sample.services.MapSorter.sortByValue;

/**
 * Created by Che on 3/28/14.
 */
public class DBManager {
    public static Connection connection;
    private PreparedStatement additionalStatement, generalStatement;
    private HashMap<String, Integer> journals = new HashMap<>();
    private static DBType currentDbType = DBType.EMBEDDED_MYSQL;

    private static String currentTable;
    private Map<String, String> tables;
    private String insertUniqueWordsQuery = "INSERT DELAYED IGNORE INTO unique_words(word) VALUE (?)";
    private String autocompleteQuery = "SELECT WORD FROM unique_words WHERE WORD LIKE ? LIMIT 10";
    private String selectQuery, insertQuery, createTableQuery, createWordsTableQuery;
    private int wordPagination = 100, paragraphPagination = 10;

    public DBManager() {
        connect(currentDbType);
        setTables(DBList.getDBList());
    }

    public void insert(WordModel paragraph) throws SQLException {
        insertQuery = "INSERT DELAYED INTO " + DBManager.getCurrentTable() + "(DOCUMENT, CHAPTER, TOPIC, PRG, DATA) VALUES (?, ?, ?, ?, ?)";
        generalStatement = connection.prepareStatement(insertQuery);
        connection.setAutoCommit(false);
        generalStatement.setString(1, paragraph.getJournal());
        generalStatement.setString(2, paragraph.getChapter());
        generalStatement.setString(3, paragraph.getTopic());
        generalStatement.setInt(4, paragraph.getPrg());
        generalStatement.setString(5, paragraph.getSentence());
        generalStatement.execute();
        connection.commit();
    }

    public void insertWords(ArrayList<String> corpus) {
        try {
            String insertWordsQuery = "INSERT DELAYED INTO " + DBManager.getCurrentTable() + "_words" + "(WORD) VALUES (?)";
            generalStatement = connection.prepareStatement(insertWordsQuery);
            additionalStatement = connection.prepareStatement(insertUniqueWordsQuery);
            Iterator<String> iterator = corpus.iterator();
            connection.setAutoCommit(false);
            while (iterator.hasNext()) {
                String word = iterator.next();
                generalStatement.setString(1, word);
                generalStatement.execute();
                additionalStatement.setString(1, word);
                additionalStatement.execute();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<JournalModel> getParagraphsByWord(String word) {
        List response = new ArrayList<JournalModel>();
        for (Map.Entry<String, String> table : tables.entrySet()) {
            ResultSet resultSet;
            selectQuery = "SELECT DOCUMENT, CHAPTER, TOPIC, PRG, DATA FROM " + table.getValue() + " WHERE DATA REGEXP '[^А-Яа-я]" + word + "' OR DATA LIKE ? LIMIT ?";
            try {
                generalStatement = connection.prepareStatement(selectQuery, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                generalStatement.setString(1, word + "%");
                generalStatement.setInt(2, paragraphPagination);
                resultSet = generalStatement.executeQuery();
                while (resultSet.next()) {
                    response.add(
                            new JournalModel(
                                    new JournalInfo(
                                            resultSet.getString(1),
                                            resultSet.getString(2),
                                            resultSet.getString(3),
                                            resultSet.getInt(4)),
                                    resultSet.getString(5)));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    public Map getWords(String word) {
        HashMap<String, Integer> words = new HashMap<>();
        for (Map.Entry<String, String> table : tables.entrySet()) {
            selectQuery = "SELECT COUNT( word ) AS corp, word FROM " + table.getValue() + "_words" + " WHERE word LIKE '" + word + "%' GROUP BY word ORDER BY corp DESC LIMIT ?";
            ResultSet resultSet;
            try {
                generalStatement = connection.prepareStatement(selectQuery, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                generalStatement.setInt(1, wordPagination);
                resultSet = generalStatement.executeQuery();
                while (resultSet.next()) {
                    if (words.containsKey(resultSet.getString(2))) {
                        words.put(resultSet.getString(2), words.get(resultSet.getString(2)) + resultSet.getInt(1));
                    } else {
                        words.put(resultSet.getString(2), resultSet.getInt(1));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return sortByValue(words);
    }

    public HashMap<String, Integer> getGraphicalStatistics(String word) {
        for (Map.Entry<String, String> table : tables.entrySet()) {
            selectQuery = "SELECT COUNT(WORD) FROM " + table.getValue() + "_words" + " WHERE WORD = ?";
            ResultSet resultSet;
            try {
                generalStatement = connection.prepareStatement(
                        selectQuery,
                        ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                generalStatement.setString(1, word);
                resultSet = generalStatement.executeQuery();
                resultSet.next();
                journals.put(table.getKey(), resultSet.getInt(1));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return journals;
    }

    public List getAutoComplete(String text) {
        List words = new ArrayList<>();
        ResultSet resultSet;
        try {
            generalStatement = connection.prepareStatement(
                    autocompleteQuery,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            generalStatement.setString(1, text + "%");

            resultSet = generalStatement.executeQuery();
            while (resultSet.next()) {
                words.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Разрыв соединения!");
            System.out.println("Подключение...");
            connect(currentDbType);
        }
        List<String> results = words.subList(0, words.size());
        Collections.sort(results);
        return results;
    }

    public void createDataTable(String dataTableName) {
        this.setCurrentTable(dataTableName);
        createTableQuery = "CREATE TABLE IF NOT EXISTS `" +
                dataTableName + "_words` (id int(11) NOT NULL AUTO_INCREMENT, " +
                "word varchar(50) COLLATE utf8_unicode_ci NOT NULL, " +
                "PRIMARY KEY (id)) ENGINE=MyISAM " +
                "DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1";

        createWordsTableQuery = "CREATE TABLE IF NOT EXISTS `" + dataTableName + "` (" +
                "id int(11) NOT NULL AUTO_INCREMENT," +
                "document varchar(100) COLLATE utf8_unicode_ci NOT NULL," +
                "chapter varchar(300) COLLATE utf8_unicode_ci NOT NULL," +
                "topic varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL," +
                "prg int(11) NOT NULL," +
                "data text COLLATE utf8_unicode_ci NOT NULL," +
                "PRIMARY KEY (id)" +
                ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
        try {
            connection.setAutoCommit(false);
            generalStatement = connection.prepareStatement(createTableQuery);
            generalStatement.execute();
            generalStatement = connection.prepareStatement(createWordsTableQuery);
            generalStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setTables(Map<String, String> tables) {
        this.tables = tables;
    }

    public static String getCurrentTable() {
        return currentTable;
    }

    public static void setCurrentTable(String currentTable) {
        DBManager.currentTable = currentTable;
    }

    public int getWordPagination() {
        return wordPagination;
    }

    public void setWordPagination(int wordPagination) {
        this.wordPagination = wordPagination;
    }

    public int getParagraphPagination() {
        return paragraphPagination;
    }

    public void setParagraphPagination(int paragraphPagination) {
        this.paragraphPagination = paragraphPagination;
    }

    private void connect(DBType dbType) {
        try {
            connection = DBUtils.getConnection(dbType);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void closeConnection() {
        try {
            switch (currentDbType) {
                case MYSQL:
                    connection.close();
                    break;
                case EMBEDDED_MYSQL:
                    connection.close();
                    DBUtils.mysqlServer.stopServer();
                    break;
                case HSQLDB:
                    connection.close();
                    break;
                default:
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}