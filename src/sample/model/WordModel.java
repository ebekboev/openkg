package sample.model;

/**
 * Created by Che on 4/11/14.
 */
public class WordModel {
	String journal;
	String chapter;
	String topic;
	int prg;
	String sentence;

	public WordModel() {
	}

	public WordModel(String journal, String chapter, String topic, int prg, String sentence) {
		this.journal = journal;
		this.chapter = chapter;
		this.topic = topic;
		this.prg = prg;
		this.sentence = sentence;
	}

	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getChapter() {
		return chapter;
	}

	public void setChapter(String chapter) {
		this.chapter = chapter;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getPrg() {
		return prg;
	}

	public void setPrg(int prg) {
		this.prg = prg;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}
}
