package sample.model;

/**
 * Created by Che on 3/28/14.
 */
public enum DBType {
	HSQLDB, MYSQL, EMBEDDED_MYSQL
}
