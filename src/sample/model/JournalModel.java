package sample.model;

public class JournalModel {
    private JournalInfo journalInfo;
    private String sentence;

    public JournalModel() {
    }

    public JournalModel(JournalInfo journalInfo, String sentence) {
        this.journalInfo = journalInfo;
        this.sentence = sentence;
    }

    public JournalInfo getJournalInfo() {
        return journalInfo;
    }

    public void setJournalInfo(JournalInfo journalInfo) {
        this.journalInfo = journalInfo;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}