package sample.model;

import sample.services.MapSorter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marvell on 28.05.2015.
 */
public class DBList {
    private static Map<String, String> tables;
    private static String selectQuery, insertQuery;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    public static Map<String, String> getDBList() {
        selectQuery = "SELECT NAME, DB_NAME FROM LIST";
        try {
            preparedStatement = DBManager.connection.prepareStatement(selectQuery);
            resultSet = preparedStatement.executeQuery();
            tables = new HashMap<>();
            while (resultSet.next()) {
                tables.put(resultSet.getString(1), resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tables;
    }

    public static void addTable(String tableName) {
        insertQuery = "INSERT INTO LIST(NAME, DB_NAME) VALUES(?, ?)";
        try {
            preparedStatement = DBManager.connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, tableName);
            preparedStatement.setString(2, MapSorter.cirylicToEnglishTranlit(tableName));
            preparedStatement.execute();
            DBManager.connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
