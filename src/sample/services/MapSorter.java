package sample.services;

import java.util.*;

/**
 * Created by Marvell on 30.05.2015.
 */
public class MapSorter {
    public static Map sortByValue(Map unsorted) {
        List list = new LinkedList(unsorted.entrySet());

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public static String cirylicToEnglishTranlit(String text) {
        text = text.toLowerCase();
        char[] abcCyr = {'а', ' ', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'ң', 'о', 'ө', 'п', 'р', 'с', 'т', 'у', 'ү', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ы', 'ъ', 'ь', 'э', 'ю', 'я',
                'А', ' ', '-', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'Ң', 'О', 'Ө', 'П', 'Р', 'С', 'Т', 'У', 'Ү', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ы', 'Ъ', 'Ь', 'Э', 'Ю', 'Я'
        };
        String[] abcLat = {"a", "_", "b", "v", "g", "d", "e", "jo", "j", "z", "i", "yi", "k", "l", "m", "n", "n", "o", "yo", "p", "r", "s", "t", "u", "yu", "f", "h", "ts", "ch", "sh", "sch", "y", "", "", "e", "ju", "ja",
                "A", "_", "", "B", "V", "G", "D", "E", "JO", "J", "Z", "I", "YI", "K", "L", "M", "N", "N", "O", "YO", "P", "R", "S", "T", "U", "YU", "F", "H", "TS", "CH", "SH", "SCH", "Y", "", "", "E", "JU", "JA"
        };
        StringBuilder english = new StringBuilder();
        outer:
        for (int i = 0; i < text.length(); i++) {
            for (int x = 0; x < abcCyr.length; x++)
                if (text.charAt(i) == abcCyr[x]) {
                    english.append(abcLat[x]);
                    continue outer;
                }
            english.append(text.charAt(i));
        }
        return english.toString();
    }
}
