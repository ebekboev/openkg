package sample;

import sample.model.DBType;
import sample.model.DBUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by marvell on 3/8/15.
 */
public class WordStatistics {
    public static void main(String[] args) {
        Connection connection;
        String query;
        PreparedStatement statement;

        HashMap<Integer, Integer> stat = new HashMap<>();

        try {
            Class.forName(DBUtils.H_DRIVER);
            connection = DBUtils.getConnection(DBType.HSQLDB);
            query = "SELECT WORD FROM CORPUS";
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();


//            while (result.next()) {
//                String word = result.getString(1);
//                Integer wordSize = word.length();
////                if (wordSize == (Integer)69) {
//                    System.out.println(word);
////                }
//                if (stat.containsKey(wordSize)) {
//                    stat.put(wordSize, stat.get(wordSize) + 1);
//                } else {
//                    stat.put(wordSize, 1);
//                }
//            }

            Iterator iterator = stat.entrySet().iterator();

            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;

    }
}
