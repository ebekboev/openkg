package sample;

import java.beans.Statement;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.mysql.management.MysqldResource;
import com.mysql.management.MysqldResourceI;
import com.mysql.management.util.QueryUtil;

public class HibernateTest {
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static final String JAVA_IO_TMPDIR = "java.io.tmpdir";

    public static void main(String[] args) throws Exception {
        File ourAppDir = new File(System.getProperty(JAVA_IO_TMPDIR));
        File databaseDir = new File(ourAppDir, "mysql-mxj");
        int portNumber = Integer.parseInt(System.getProperty("c-mxj_test_port", "3306"));
        String userName = "olyanren";
        String password = "1987";
        MysqldResource mysqldResource = startDatabase(databaseDir, portNumber,
                userName, password);
        Class.forName(DRIVER);
        Connection conn = null;
        String dbName = "journals";
        String url = "jdbc:mysql://localhost:" + portNumber + "/" + dbName
                + "?" + "createDatabaseIfNotExist=true";
        conn = DriverManager.getConnection(url, userName, password);

        System.out.println("------------------------");
        System.out.println(new File(".").getCanonicalPath());
        System.out.println(System.getProperty("user.dir"));
        System.out.println("------------------------");
        System.out.println();
//        System.out.println(ourAppDir.getPath());
        String route = databaseDir.getPath() + "\\bin\\";
        System.out.println(route + "mysql -u olyanren -p1987 -e \"use journals; source D://journals.sql;\"");
        Runtime.getRuntime().exec(route + "mysql -u olyanren -p1987 -e \"use journals; source D://journals.sql;\"");

        PreparedStatement statement = conn.prepareStatement("SELECT SENTENCE FROM AKBASHAT", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            System.out.println(resultSet.getString(1));
        }
    }

    public static MysqldResource startDatabase(File databaseDir, int port, String userName, String password) {
        MysqldResource mysqldResource = new MysqldResource(databaseDir);
        Map database_options = new HashMap();
        database_options.put(MysqldResourceI.PORT, Integer.toString(port));
        database_options.put(MysqldResourceI.INITIALIZE_USER, "true");
        database_options.put(MysqldResourceI.INITIALIZE_USER_NAME, userName);
        database_options.put(MysqldResourceI.INITIALIZE_PASSWORD, password);

        mysqldResource.start("test-mysqld-thread", database_options);
        if (!mysqldResource.isRunning()) {
            throw new RuntimeException("MySQL did not start.");
        }
        System.out.println("MySQL is running.");
        return mysqldResource;
    }
}