package sample;

import sample.model.DBType;
import sample.model.DBUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by marvell on 2/23/15.
 */
public class UniqueStatistics {
    public static void main(String[] args) {
        Connection connection;
        String query;
        PreparedStatement statement;
        int lettersCount = 0;
        char[] alpha = "абвгдеёжзийклмнңоөпрстуүфхцчшщъыьэюя".toCharArray();
        HashMap<Character, Integer> total = new HashMap<>();
        HashMap<Character, Integer> begin = new HashMap<>();
        HashMap<Character, Integer> end = new HashMap<>();
        for (int i = 0; i < alpha.length; i++) {
            total.put(alpha[i], 0);
            begin.put(alpha[i], 0);
            end.put(alpha[i], 0);
        }

        try {
            Class.forName(DBUtils.H_DRIVER);
            connection = DBUtils.getConnection(DBType.HSQLDB);
            query = "SELECT DISTINCT WORD FROM CORPUS";
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();


            while (result.next()) {
                char[] word = result.getString(1).toCharArray();
                for (int i = 0; i < word.length; i++) {
                    if (total.containsKey(word[i])) {
                        if (i == 0) {
                            begin.put(word[i], begin.get(word[i]) + 1);
                        } else if (i == word.length - 1) {
                            end.put(word[i], end.get(word[i]) + 1);
                        }
                        total.put(word[i], total.get(word[i]) + 1);
                        lettersCount++;
                    }
                }
            }

            System.out.println("Всего букв: " + lettersCount);


            System.out.println("Общее: ");

            for (int i = 0; i < alpha.length; i++) {
                System.out.println(total.get(alpha[i]));
            }

            System.out.println("В начале слова: ");

            for (int i = 0; i < alpha.length; i++) {
                System.out.println(begin.get(alpha[i]));
            }

            System.out.println("В конце слова: ");

            for (int i = 0; i < alpha.length; i++) {
                System.out.println(end.get(alpha[i]));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;

    }

}
