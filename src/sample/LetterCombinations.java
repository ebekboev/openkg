package sample;

import sample.model.DBType;
import sample.model.DBUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by marvell on 2/17/15.
 */
public class LetterCombinations {
    public static void main(String[] args) {
        Connection connection;
        String query;
        PreparedStatement statement;

        char[] alpha = "абвгдеёжзийклмнңоөпрстуүфхцчшщъыьэюя".toCharArray();
        HashMap<String, Integer> combinations = new HashMap<>();
        for (int i = 0; i < alpha.length; i++) {
            for (int j = 0; j < alpha.length; j++) {
                combinations.put(Character.toString(alpha[i]) + Character.toString(alpha[j]), 0);
            }
        }

        try {
            Class.forName(DBUtils.H_DRIVER);
            connection = DBUtils.getConnection(DBType.HSQLDB);
            query = "SELECT WORD FROM CORPUS";
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();


            while (result.next()) {
                String word = result.getString(1);
                for (int i = 0; i < word.length() - 1; i++) {
                    if (combinations.containsKey(word.substring(i, i + 2))) {
                        combinations.put(word.substring(i, i + 2), combinations.get(word.substring(i, i + 2)) + 1);
                    }
                }
            }

            for (int i = 0; i < alpha.length; i++) {
                System.out.print(" " + Character.toUpperCase(alpha[i]));
            }
            System.out.println();

            for (int i = 0; i < alpha.length; i++) {
                System.out.print(Character.toUpperCase(alpha[i]) + " ");
                for (int j = 0; j < alpha.length; j++) {
                    System.out.print(combinations.get(Character.toString(alpha[i]) + Character.toString(alpha[j])) + " ");
                }
                System.out.println();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;
    }
}
