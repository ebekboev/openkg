package sample.controller;

import org.apache.commons.io.FileUtils;
import sample.model.JournalModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Che on 3/28/14.
 */
public class TextParser {
	private File file;
	private ArrayList<JournalModel> resultText;
	private List<String> data;
	private JournalModel entity;

	public TextParser(File file) {
		this.file = file;
	}

	public void parse() {
		int journal=1, chapter=1, topic=1, paragraph=1;
		String jrnl="", chptr="", tpc="", string, type;
		try{
			data = FileUtils.readLines(file);
			File out = new File("D://out.txt");
			resultText = new ArrayList<>();
			Iterator<String> iterator = data.iterator();

			while (iterator.hasNext()) {
				entity = new JournalModel();
				string = iterator.next();
				if (string.length() > 1) {
					System.out.println(string);
					type = string.substring(0, 1);
					if (type.equals("@")) {//Название журнала
						jrnl = string.substring(2);
						FileUtils.writeStringToFile(out, journal + string + "\n", true);
						journal++;
						chapter = 1;
						topic = 1;
						paragraph = 0;
						chptr = null;
						tpc = null;
					} else if (type.equals("#")) {//Название раздела
						chptr = string.substring(2);
						FileUtils.writeStringToFile(out, chapter + string + "\n", true);
						chapter++;
						topic = 1;
						paragraph = 0;
						tpc = null;
					} else if (type.equals("&")) {//Название топика
						tpc = string.substring(2);
						FileUtils.writeStringToFile(out, topic + string + "\n", true);
						topic++;
						paragraph = 0;
					} else if (type.equals("$")) {//Параграф
						++paragraph;
						FileUtils.writeStringToFile(out, paragraph + string + "\n", true);
//						entity.setJournalInfo(jrnl);
//						entity.setChapter(chptr);
//						entity.setTopic(tpc);
//						entity.setParagraph(paragraph);
						entity.setSentence(string.substring(2));
						resultText.add(entity);
					} else {//Предложение
						FileUtils.writeStringToFile(out, string + "\n", true);
//						entity.setJournalInfo(jrnl);
//						entity.setChapter(chptr);
//						entity.setTopic(tpc);
//						entity.setParagraph(paragraph);
						entity.setSentence(string);
						resultText.add(entity);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public List getData() {
		return resultText;
	}
}
