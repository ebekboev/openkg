package sample.controller;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.controlsfx.control.textfield.TextFields;
import sample.eu.hansolo.fx.SlideCheckBox;
import sample.model.*;
import sample.services.MapSorter;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Consumer;

public class MainController implements Initializable {
    public DBManager manager;
    public JournalService journalService = new JournalService(this);
    public CorpsService corpsService = new CorpsService(this);
    public ProgressIndicator progressIndicator = new ProgressIndicator();
    public LineChart<String, Number> lineChart;
    public CategoryAxis horizontalAx;
    public NumberAxis verticalAx;
    public @FXML MenuItem corps_search;
    public @FXML MenuItem frequency_search;
    public @FXML MenuItem importWord;
    public @FXML StackPane stack;
    public @FXML TableColumn source;
    public @FXML TableColumn sentence;
    public @FXML TableView journalTable;
    public @FXML TableView corpusTable;
    public @FXML Button searchButton;
    public @FXML Button paginationButton;
    public @FXML TextField searchTextField;
    public @FXML Text bigN;
    public @FXML Text bigO;
    public @FXML Text bigY;
    public @FXML Text smlN;
    public @FXML Text smlO;
    public @FXML Text smlY;
    public @FXML Text quantity;
    public @FXML TableColumn frequency;
    public @FXML TableColumn word;
    public @FXML HBox togglePane;
    public @FXML GridPane bottomPane;
    public Region veil = new Region();
    public List<File> files;
    public Parent root;
    public FXMLLoader loader;
    Text type;
    int pos, paragraphs, words;
    String content, currentWord = null, currentParagraph = null, newCorpusname;
    IndexRange range;
    SlideCheckBox checkBox;
    private ObservableList<String> data = FXCollections.observableArrayList();
    private ObservableList<CorpsModel> corpusList = FXCollections.observableArrayList();
    private ObservableList<JournalModel> journalList = FXCollections.observableArrayList();
    private WordController wordController;
    private FileChooser chooser;
    private long requestTimeLimit;
    private boolean containsTable = false;
    private String duplicateTitle;

    public MainController() {
        manager = new DBManager();
    }

    @FXML
    public void insertExtra(MouseEvent event) {
        type = (Text) event.getSource();
        pos = searchTextField.getCaretPosition();
        String text = type.getText();
        content = searchTextField.getText();

        if (searchTextField.isFocused()) {
            range = searchTextField.getSelection();
            content = content.substring(0, range.getStart()) + text + content.substring(range.getEnd());
            searchTextField.setText(content);
            searchTextField.positionCaret(pos + 1);
        } else {
            searchTextField.requestFocus();
            searchTextField.setText(String.valueOf(text));
            searchTextField.positionCaret(pos + 1);
        }
    }

    @FXML
    public void searchText() {

        if ((System.currentTimeMillis() - requestTimeLimit) < 500) {
            return;
        }

        requestTimeLimit = System.currentTimeMillis();
        if (corpusTable.isVisible()) {
            currentWord = searchTextField.getText();
            manager.setWordPagination(10);
            corpsService.restart();
            paginationButton.setVisible(true);
        } else if (journalTable.isVisible()) {
            currentParagraph = searchTextField.getText();
            manager.setParagraphPagination(10);
            paginationButton.setVisible(true);
            journalService.restart();
        } else {
            if (!checkBox.isSelected()) {
                lineChart.getData().clear();
            } else {
                for (int i = 0; i < lineChart.getData().size(); i++) {
                    if (lineChart.getData().get(i).getName().equals(searchTextField.getText())) {
                        lineChart.getData().remove(i);
                    }
                }
            }
            HashMap<String, Integer> journals = manager.getGraphicalStatistics(searchTextField.getText());
            XYChart.Series series = new XYChart.Series();
            series.setName(searchTextField.getText());
            for (Map.Entry<String, Integer> journal : journals.entrySet()) {
                series.getData().add(new XYChart.Data(journal.getKey(), journal.getValue()));
            }
            lineChart.getData().add(series);
        }
    }

    @FXML
    public void showAboutApp() {
        loader = new FXMLLoader(MainController.class.getResource("/sample/view/about-copus.fxml"));
        try {
            Parent parent = loader.load();
            Stage aboutStage = new Stage();
            aboutStage.initModality(Modality.NONE);
            aboutStage.getIcons().add(new Image("/sample/view/dictionary.png"));
            aboutStage.setTitle("Программа жөнүндө");
            aboutStage.setResizable(false);
            aboutStage.setScene(new Scene(parent));
            aboutStage.show();
            aboutStage.requestFocus();
            aboutStage.focusedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    aboutStage.close();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void importWordDocument() {
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Импорт");
        dialog.setHeaderText("Корпусту берилиштер базасына сактоо үчүн\n" +
                "ага уникалдуу ат берилиш керек!");
        dialog.setGraphic(new ImageView(this.getClass().getResource("/sample/view/DOC.png").toString()));
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/sample/view/dictionary.png")));

        ButtonType addCorpusButton = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(addCorpusButton);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField corpus = new TextField();
        corpus.setPromptText("Корпус");

        grid.add(new Label("Корпустун аты: "), 0, 0);
        grid.add(corpus, 1, 0);

        Node loginButton = dialog.getDialogPane().lookupButton(addCorpusButton);
        loginButton.setDisable(true);

        corpus.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                corpus.requestFocus();
            }
        });

        dialog.setResultConverter(new Callback<ButtonType, String>() {
            @Override
            public String call(ButtonType dialogButton) {
                if (dialogButton == addCorpusButton) {
                    return corpus.getText();
                }
                return null;
            }
        });

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(new Consumer<String>() {
            @Override
            public void accept(String corpusName) {
                newCorpusname = corpusName;
                for (String title : data) {
                    if (title.toLowerCase().equals(newCorpusname.toLowerCase())) {
                        DBManager.setCurrentTable(MapSorter.cirylicToEnglishTranlit(newCorpusname));
                        containsTable = true;
                        duplicateTitle = title;
                        break;
                    }
                }
            }
        });

        if (!result.isPresent()) {
            return;
        }

        chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("MS Word Files", "*.docx"));
        files = chooser.showOpenMultipleDialog(null);
        if (files != null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Импорт");
            alert.setHeaderText(files.size() + " документ тандалды.\n" + (containsTable ? duplicateTitle + " корпусу толукталат." : "Жаңы корпус түзүлөт."));
            alert.setContentText("Конвертациялоо процесси учурунда программаны жабууга тыйуу салынат! Улантууга даярсыңар бы?");
            alert.showAndWait();

            ButtonType buttonType = alert.getResult();
            if (buttonType == null) {
                return;
            } else if (!buttonType.getText().equals("OK")) {
                return;
            }
            if (!containsTable) {
                FXCollections.sort(data);
                manager.createDataTable(MapSorter.cirylicToEnglishTranlit(newCorpusname));
                DBList.addTable(newCorpusname);
                manager.setTables(DBList.getDBList());
                data.add(newCorpusname);
            }
            containsTable = false;

            loader = new FXMLLoader(MainController.class.getResource("/sample/view/wordimport.fxml"));
            try {
                root = loader.load();
                wordController = loader.getController();
                wordController.setFiles(files);
                wordController.setController(this);
                stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.getIcons().add(new Image("/sample/view/dictionary.png"));
                stage.setTitle("MS Word Импорт");
                stage.setResizable(false);
                stage.setScene(new Scene(root));
                stage.show();
                wordController.restartTask();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return;
        }
    }

    @FXML
    public void showJournalSearchForm() {
        if (currentParagraph != null) {
            paginationButton.setVisible(true);
        } else {
            paginationButton.setVisible(false);
        }
        quantity.setText(String.format("%d параграф", paragraphs));
        quantity.setVisible(true);
        togglePane.setVisible(false);
        corpusTable.setVisible(false);
        journalTable.setVisible(true);
        progressIndicator.progressProperty().bind(journalService.progressProperty());
        veil.visibleProperty().bind(journalService.runningProperty());
        progressIndicator.visibleProperty().bind(journalService.runningProperty());
    }

    @FXML
    public void showFreqSearchForm() {
        if (currentWord != null) {
            paginationButton.setVisible(true);
        } else {
            paginationButton.setVisible(false);
        }
        quantity.setText(String.format("%d сөз", words));
        quantity.setVisible(true);
        togglePane.setVisible(false);
        corpusTable.setVisible(true);
        journalTable.setVisible(false);
        progressIndicator.visibleProperty().bind(corpsService.runningProperty());
        progressIndicator.progressProperty().bind(corpsService.progressProperty());
        veil.visibleProperty().bind(corpsService.runningProperty());
    }

    @FXML
    public void showGraphStatForm() {
        paginationButton.setVisible(false);
        quantity.setVisible(false);
        togglePane.setVisible(true);
        corpusTable.setVisible(false);
        journalTable.setVisible(false);
        quantity.setVisible(false);
    }

    @FXML
    public void exitAction() throws SQLException {
        DBManager.closeConnection();
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        searchButton.disableProperty().bind(Bindings.isEmpty(searchTextField.textProperty()));

        checkBox = new SlideCheckBox();
        checkBox.setScaleX(0.4);
        checkBox.setScaleY(0.4);
        checkBox.setSelected(true);
        togglePane.getChildren().add(checkBox);
        togglePane.setVisible(false);

        paginationButton.setOnAction(event -> ajaxLoad());
        paginationButton.setVisible(false);

        for (Map.Entry<String, String> table : DBList.getDBList().entrySet()) {
            data.add(table.getKey());
        }
        FXCollections.sort(data);

        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                int listSize = lineChart.getData().size();
                if (!newValue && listSize > 1) {
                    XYChart.Series<String, Number> lastRow = lineChart.getData().get(listSize - 1);
                    XYChart.Series series = new XYChart.Series();
                    for (XYChart.Data<String, Number> data : lastRow.getData()) {
                        series.getData().add(new XYChart.Data(data.getXValue(), data.getYValue()));
                    }
                    lineChart.getData().clear();
                    series.setName(lastRow.getName());
                    lineChart.getData().add(series);
                }
            }
        });

        horizontalAx.setCategories(data);
        horizontalAx.setTickLabelRotation(-45);
        horizontalAx.setTickMarkVisible(true);
        horizontalAx.setTickLabelFont(new Font(10));
        horizontalAx.setLabel("Журнал");
        verticalAx.setLabel("Саны");
        lineChart.setTitle("Графикалык статистика");

        journalTable.setPlaceholder(new Text("Жадыбал бош!"));
        corpusTable.setPlaceholder(new Text("Жадыбал бош!"));
        corpusTable.setVisible(false);
        corpusTable.setItems(corpusList);
        corpsService.valueProperty().addListener(new ChangeListener<ObservableList<CorpsModel>>() {
            @Override
            public void changed(ObservableValue<? extends ObservableList<CorpsModel>> observable, ObservableList<CorpsModel> oldValue, ObservableList<CorpsModel> newValue) {
                corpusTable.refresh();
                if (oldValue != null) {
                    corpusList.removeAll(oldValue);
                }
                if (newValue != null) {
                    corpusList.addAll(newValue);
                }
            }
        });
        frequency.setCellValueFactory(new PropertyValueFactory<CorpsModel, Integer>("frequency"));
        frequency.setCellFactory(corpsModelIntegerTableColumn -> {
            TableCell<CorpsModel, Integer> cell = new TableCell<CorpsModel, Integer>() {
                @Override
                public void updateItem(Integer item, boolean empty) {
                    if (item != null && !empty) {
                        setStyle("-fx-font-size: 25");
                        setAlignment(Pos.CENTER);
                        setText(String.valueOf(item));
                    }
                }
            };
            return cell;
        });
        word.setCellValueFactory(new PropertyValueFactory<CorpsModel, String>("word"));
        word.setCellFactory(corpsModelIntegerTableColumn -> {
            TableCell<CorpsModel, String> cell = new TableCell<CorpsModel, String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    if (item != null && !empty) {
                        setStyle("-fx-fill: #5160e2; -fx-font-size: 25; -fx-font-weight: bold; -fx-font-style: italic");
                        setAlignment(Pos.CENTER);
                        setText(item);
                    }
                }
            };
            return cell;
        });

        TextFields.bindAutoCompletion(searchTextField, param -> searchTextField.getText().length() > 2 ? manager.getAutoComplete(searchTextField.getText()) : null);

        veil.setStyle("-fx-background-color: rgba(0, 0, 0, 0.4)");
        progressIndicator.setId("progress");
        progressIndicator.setMaxSize(300, 300);
        progressIndicator.progressProperty().addListener((observableValue, number, number2) -> {
            if (number2.doubleValue() >= 1) {
                Text text = (Text) progressIndicator.lookup(".percentage");
                text.setText("100%");
            }
        });
        progressIndicator.progressProperty().bind(journalService.progressProperty());
        veil.visibleProperty().bind(journalService.runningProperty());
        progressIndicator.visibleProperty().bind(journalService.runningProperty());
        journalTable.setItems(journalList);
        journalService.valueProperty().addListener(new ChangeListener<ObservableList<JournalModel>>() {
            @Override
            public void changed(ObservableValue<? extends ObservableList<JournalModel>> observable, ObservableList<JournalModel> oldValue, ObservableList<JournalModel> newValue) {
                journalTable.refresh();
                if (oldValue != null) {
                    journalList.removeAll(oldValue);
                }
                if (newValue != null) {
                    journalList.addAll(newValue);
                }
            }
        });
        journalTable.itemsProperty().bind(journalService.valueProperty());
        stack.getChildren().addAll(veil, progressIndicator);

        source.setMaxWidth(1366);
        source.setCellValueFactory(new PropertyValueFactory<JournalModel, JournalInfo>("journalInfo"));
        source.setCellFactory(journalStringTableColumn -> {
            TableCell<JournalModel, JournalInfo> cell = new TableCell<JournalModel, JournalInfo>() {

                @Override
                public void updateItem(JournalInfo item, boolean empty) {
                    if (item != null && !empty) {
                        Text text1, text2, text3, text4;
                        VBox pane = new VBox();
                        text1 = new Text("ДОКУМЕНТ: " + item.getJournal());
                        text1.setStyle("-fx-fill: #3f413e; -fx-font-size: 15; -fx-font-weight: bold; -fx-font-style: italic");
                        text1.wrappingWidthProperty().bind(source.widthProperty());
                        pane.getChildren().add(text1);
                        text2 = new Text("БӨЛҮМ: " + item.getChapter());
                        text2.setStyle("-fx-fill: #d26f2d; -fx-font-size: 13; -fx-font-weight: bold");
                        text2.wrappingWidthProperty().bind(source.widthProperty());
                        pane.getChildren().add(text2);
                        if (!item.getChapter().equals(item.getTopic())) {
                            text3 = new Text("БӨЛҮК : " + item.getTopic());
                            text3.setStyle("-fx-fill: #2c2ab0; -fx-font-size: 13; -fx-font-weight: bold");
                            text3.wrappingWidthProperty().bind(source.widthProperty());
                            pane.getChildren().add(text3);
                        }
                        text4 = new Text("АБЗАЦ : " + item.getPrg());
                        text4.setStyle("-fx-fill: #086615; -fx-font-size: 13; -fx-font-weight: bold");
                        text4.wrappingWidthProperty().bind(source.widthProperty());
                        pane.getChildren().add(text4);
                        setGraphic(pane);
                    }
                }
            };
            return cell;
        });

        sentence.setCellValueFactory(new PropertyValueFactory<JournalModel, String>("sentence"));
        sentence.setCellFactory(journalStringTableColumn -> {
            TableCell<JournalModel, String> cell = new TableCell<JournalModel, String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    if (searchTextField.getText().isEmpty()) {
                        Text text = new Text(item);
                        text.setStyle("-fx-fill: #515350; -fx-font-size: 18; -fx-font-style: italic");
                        text.wrappingWidthProperty().bind(sentence.widthProperty());
                        final HBox flow = new HBox(text);
                        setGraphic(flow);
                    } else if (item != null && !empty) {
                        HBox hbox = new HBox();
                        int index = 0, size = searchTextField.getText().length();
                        ArrayList<Node> textList = new ArrayList<Node>();
                        while (true) {
                            int s = item.toLowerCase().indexOf(searchTextField.getText().toLowerCase(), index);
                            if (s != -1) {
                                Text text = new Text(item.substring(index, s));
                                text.setStyle("-fx-fill: #383a37; -fx-font-size: 20; -fx-font-style: italic");
                                Text text1 = new Text(item.substring(s, s + size));
                                text1.setStyle("-fx-fill: #de020a; -fx-font-size: 20; -fx-font-weight: bold; -fx-font-style: italic");
                                textList.add(text);
                                textList.add(text1);
                                index = s + searchTextField.getText().length();
                            } else {
                                Text text = new Text(item.substring(index, item.length()) + "\n");
                                text.setStyle("-fx-fill: #383a37; -fx-font-size: 20; -fx-font-style: italic");
                                textList.add(text);
                                break;
                            }
                        }
                        Node[] nodes = new Node[textList.size()];
                        Node[] n = textList.toArray(nodes);
                        TextFlow clone = new TextFlow(n);
                        prefHeightProperty().bind(clone.heightProperty());
                        setMinHeight(prefHeightProperty().getValue());
                        setMaxHeight(prefHeightProperty().getValue());
                        hbox.getChildren().add(clone);
                        setGraphic(hbox);
                    }
                }
            };
            return cell;
        });
    }

    public void ajaxLoad() {
        if (corpusTable.isVisible()) {
            manager.setWordPagination(manager.getWordPagination() + 100);
            corpsService.restart();
        } else if (journalTable.isVisible()) {
            manager.setParagraphPagination(manager.getParagraphPagination() + 10);
            journalService.restart();
        }
    }

    public void setWords(int words) {
        quantity.setText(String.format("%d сөз", words));
        this.words = words;
    }

    public void setParagraphs(int paragraphs) {
        quantity.setText(String.format("%d параграф", paragraphs));
        this.paragraphs = paragraphs;
    }
}