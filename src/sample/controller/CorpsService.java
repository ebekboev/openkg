package sample.controller;

import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import sample.model.CorpsModel;

/**
 * Created by Che on 4/14/14.
 */
public class CorpsService extends Service<ObservableList<CorpsModel>> {
	private MainController mainController;

	public CorpsService(MainController mainController) {
		this.mainController = mainController;
	}

	@Override
	protected Task<ObservableList<CorpsModel>> createTask() {
		return new CorpsTask(mainController);
	}
}
