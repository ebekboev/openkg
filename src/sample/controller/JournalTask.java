package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import sample.model.JournalModel;

import java.util.List;

/**
 * Created by Che on 3/31/14.
 */
public class JournalTask extends Task<ObservableList<JournalModel>> {

    private MainController mainController;

    public JournalTask(MainController mainController) {
        this.mainController = mainController;
    }

    @Override
    protected ObservableList<JournalModel> call() throws Exception {

        ObservableList<JournalModel> data = FXCollections.observableArrayList();

        List<JournalModel> paragraphs = mainController.manager.getParagraphsByWord(mainController.currentParagraph);
        int journalsSize = paragraphs.size();
        mainController.setParagraphs(journalsSize);
        for (int i = 0; i< journalsSize; i++) {
            data.add(paragraphs.get(i));
            Thread.sleep(20);
            updateProgress(i, journalsSize);
        }
        updateProgress(paragraphs.size(), paragraphs.size());
        return data;
    }
}
