package sample.controller;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
/**
 * Created by Che on 4/9/14.
 */
public class WordService extends Service{
	private WordController controller;

	public WordService(WordController controller) {
		this.controller = controller;
	}

	@Override
	protected Task createTask() {
		return new WordTask(controller);
	}
}