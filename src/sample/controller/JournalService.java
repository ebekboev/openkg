package sample.controller;

import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import sample.model.JournalModel;

/**
* Created by Che on 3/31/14.
*/
public class JournalService extends Service<ObservableList<JournalModel>> {

	private MainController mainController;

	public JournalService(MainController mainController) {
		this.mainController = mainController;
	}

	/**

	 * Create and return the task for fetching the data. Note that this

	 * method is called on the background thread (all other code in this

	 * application is on the JavaFX Application Thread!).

	 *

	 * @return A task

	 */

	@Override

	protected Task createTask() {

		return new JournalTask(mainController);

	}

}
