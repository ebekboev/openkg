package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import sample.model.CorpsModel;

import java.util.Map;

/**
 * Created by Che on 4/14/14.
 */
public class CorpsTask extends Task {
    MainController mainController;

    public CorpsTask(MainController mainController) {
        this.mainController = mainController;
    }

    @Override
    protected Object call() throws Exception {
        ObservableList<CorpsModel> data = FXCollections.observableArrayList();

        final Map<String, Integer> words = mainController.manager.getWords(mainController.currentWord);
        int a, i = 0;
        a = words.size();
        mainController.setWords(a);
        for (Map.Entry<String, Integer> word : words.entrySet()) {
            CorpsModel model = new CorpsModel(word.getKey(), word.getValue());
            data.add(model);
            updateProgress(i, a);
            Thread.sleep(10);
            i++;
        }
        updateProgress(a, a);
        return data;
    }
}
