package sample.controller;

import javafx.concurrent.Task;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import sample.model.DBManager;
import sample.model.WordModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Che on 4/9/14.
 */
public class WordTask extends Task {
    private DBManager manager;
    private WordController controller;
    private XWPFDocument document = null;
    private List<File> files;
    private String title;
    private String token = "\t \"0123456789γβα«»·™³²¹°|_]>♦[→µ@?×;:/.,+*<)(„‘'′`&√=…%~€$•●!≪≫”“″’—№–‒−\\";
    private String style;
    private String text;
    private int i, prg = 0;
    private int documentsCount;

    public WordTask(WordController controller) {
        this.controller = controller;
        this.files = controller.files;
        manager = controller.getController().manager;
    }

    @Override
    protected Object call() throws InterruptedException, SQLException, IOException {
        Iterator<File> filesIterator = files.iterator();
        documentsCount = files.size();
        controller.setDocumentsCount(documentsCount);
        while (filesIterator.hasNext()) {
            File current = filesIterator.next();
            WordModel model = new WordModel();
            try {
                document = new XWPFDocument(new FileInputStream(current));
            } catch (IOException e) {
                e.printStackTrace();
            }
            List<XWPFParagraph> paragraphs = document.getParagraphs();
            i = paragraphs.size();
            title = paragraphs.get(0).getText();
            controller.setJournalTitle(title);
            controller.setParagraphsCount(i);
            for (int x = 0; x < i; x++) {
                WordModel temp = null;
                ArrayList<String> a = new ArrayList<>();
                XWPFParagraph paragraph = paragraphs.get(x);
                StringTokenizer tokenizer = new StringTokenizer(paragraph.getText(), token, false);
                while (tokenizer.hasMoreTokens()) {
                    String word = tokenizer.nextToken().toLowerCase();
                    while (word.matches("-.{0,}")) {
                        word = word.substring(1);

                    }
                    while (word.matches(".{0,}-")) {
                        word = word.substring(0, word.length() - 1);
                    }
                    if (word.length() > 0) {
                        a.add(word);
                    }
                }
                manager.insertWords(a);
                style = paragraph.getStyle();
                text = paragraph.getText();
                switch (style) {
                    case "1":
                        model.setJournal(text);
                        model.setChapter("");
                        model.setTopic("");
                        model.setPrg(0);
                        model.setSentence(text);
                        temp = model;
                        prg = 1;
                        break;
                    case "2":
                        model.setChapter(text);
                        model.setTopic("");
                        model.setPrg(0);
                        model.setSentence(text);
                        temp = model;
                        controller.setChapterTitle(text);
                        prg = 1;
                        break;
                    case "3":
                        if (model.getChapter() == text) {
                            continue;
                        } else {
                            model.setTopic(text);
                            model.setPrg(0);
                            model.setSentence(text);
                            temp = model;
                            prg = 1;
                            break;
                        }
                    case "4":
                        model.setPrg(prg);
                        model.setSentence(text);
                        temp = model;
                        prg++;
                        break;
                }
                updateProgress(x, i);
                manager.insert(temp);
            }
            documentsCount -= 1;
            controller.setDocumentsCount(documentsCount);
        }
        updateProgress(i, i);
        return null;
    }
}
