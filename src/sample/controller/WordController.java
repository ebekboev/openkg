package sample.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Che on 4/10/14.
 */
public class WordController implements Initializable {
    public List<File> files;
    public @FXML ProgressIndicator wordProgress;
    public @FXML Region wordRegion;
    public @FXML Text documentsCount;
    public @FXML Text journalTitle;
    public @FXML Text paragraphsCount;
    public @FXML Text chapterTitle;

    private MainController controller;
    private WordService service = new WordService(this);

    public void restartTask() {
        service.restart();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        wordProgress.progressProperty().bind(service.progressProperty());
        wordProgress.visibleProperty().bind(service.runningProperty());
        wordRegion.visibleProperty().bind(service.runningProperty());
        wordProgress.progressProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.doubleValue() >= 1) {
                    hideWindow();
                }
            }
        });
    }

    public void setJournalTitle(String journalTitle) {
        this.journalTitle.setText(journalTitle);
    }

    public void setParagraphsCount(int paragraphs) {
        this.paragraphsCount.setText(String.valueOf(paragraphs));
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public void setChapterTitle(String chapter) {
        chapterTitle.setText(chapter);
    }

    public void setDocumentsCount(int count) {
        this.documentsCount.setText(String.valueOf(count));
    }

    public MainController getController() {
        return controller;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void hideWindow() {
        Stage stage = (Stage) journalTitle.getParent().getScene().getWindow();
        stage.close();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Импорт");
        alert.setHeaderText(files.size() + " документ сакталды.");
        alert.setContentText("Конвертациялоо процесси ийгиликтүү аяктады!\n");
        alert.showAndWait();
    }
}
