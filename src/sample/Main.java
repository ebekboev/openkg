package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sample.controller.MainController;
import sample.model.DBManager;

public class Main extends Application {
    private Stage primaryStage;
    private Parent root;
    private MainController mainController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/sample/view/main-page.fxml"));
        root = loader.load();
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/sample/view/dictionary.png")));
        primaryStage.setTitle("OpenKG");
        primaryStage.setScene(new Scene(root));
        mainController = loader.getController();
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        DBManager.closeConnection();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
