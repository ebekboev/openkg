package sample;

import java.util.StringTokenizer;

/**
 * Created by user on 31.03.2015.
 */
public class DelimeterTest {
    public static void main(String[] args) {
        String paragraph = "\tsdfsdfsdf\nfsdfsd_f\nfsdfsdf";

        StringTokenizer tokenizer = new StringTokenizer(paragraph, "\n", false);
        while (tokenizer.hasMoreTokens()) {
            String word = tokenizer.nextToken().toLowerCase();
            System.out.println(word);
        }
        System.out.println((int)' ');
    }
}

