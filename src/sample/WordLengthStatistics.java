package sample;

import sample.model.DBType;
import sample.model.DBUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by user on 31.03.2015.
 */
public class WordLengthStatistics {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
        Connection connection;
        String query;
        PreparedStatement statement;

        Class.forName(DBUtils.H_DRIVER);
        connection = DBUtils.getConnection(DBType.HSQLDB);
        query = "SELECT DATA FROM ALATOO";
        statement = connection.prepareStatement(query);
        ResultSet result = statement.executeQuery();
        while (result.next()) {
            String word = result.getString(1);
            if (word.length() > 10) {
                System.out.println(word + " " + word.length());
            }
        }
    }
}
