package sample;

import sample.model.DBType;
import sample.model.DBUtils;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by user on 31.03.2015.
 */
public class CharacterStatistics {
    public static void main(String[] args) {
        Connection connection;
        String query;
        PreparedStatement statement;

        HashMap<Integer, Integer> stat = new HashMap<>();

        try {
            Class.forName(DBUtils.M_DRIVER);
            connection = DBUtils.getConnection(DBType.HSQLDB);
            query = "SELECT DATA FROM alatoo";
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                String word = result.getString(1);
                char letters[] = word.toCharArray();
                for (int i = 0; i < letters.length; i++) {
                    if ((int) letters[i] == 172) {
                        System.out.println(result.getString(1));
                    }
                    if (stat.containsKey((int) letters[i])) {
                        stat.put((int) letters[i], stat.get((int) letters[i]) + 1);
                    } else {
                        stat.put((int) letters[i], 1);
                    }
                }
            }
            for (Map.Entry<Integer, Integer> entry : stat.entrySet()) {
                int character = entry.getKey();
                System.out.println((char) character + " : " + character + " : " + entry.getValue());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;
    }
}
